# Task 1: Docker basics

1. Download ubuntu image (18.04 LTS)

```bash
docker pull ubuntu:bionic
```

2. List all local images

```bash
docker images
```

3. List containers

```bash
docker ps --all
```

4. get system info

```bash
# in wsl
uname -mrs # 
cat /etc/*-release

docker run ubuntu:bionic uname -mrs
docker run ubuntu:bionic cat /etc/*-release
docker run centos:6 uname -mrs
docker run centos:6 uname cat /etc/*-release
```

5. start shell

```bash
docker run -it ubuntu:bionic /bin/bash
```

6. list all container

```bash
docker ps --all
```

7. install vim

```bash
apt-get update
apt-get install -y vim
vim /home/testfile.txt
```

8. close bash container

9. restart container and re-login

```bash
docker ps --all
docker restart f1f06c85ca77
docker ps
docker exec -it f1f06c85ca77 /bin/bash
```

10. create image from container

```bash
docker commit 578a0145ad02 my_image_:v1
```

11. cleanup

* WARNING! Dont do this if you have important container/images without backup

```bash
docker system prune --all
docker ps --all
```

* Start container with --rm to automatically remove the container after exit

```bash
docker run --rm -it ubuntu:bionic echo "test_1"
docker run -it ubuntu:bionic echo "test_2"
docker ps --all
```

# Task 2: mount volume

```bash
docker run -it -v /c/:/windows/ ubuntu:xenial /bin/bash
```

```
docker run -it -v /C/Users:/windows ubuntu:xenial /bin/bash
```

# Task 3: create two images

## data & analysis

* We create a few images. Base image contains a conda installation: 

```bash
cd docker/base
docker build -t base:v1 --build-arg username=twardzso .
```

* a container for data creation

```bash
cd docker/data
docker build -t data:v1 --build-arg username=twardzso .
docker run -p 8000:8000 data:v1 Rscript /script/start_service.R
```

* a container for some analysis

```bash
cd docker/analysis
docker build -t analysis:v1 --build-arg username=twardzso .
docker run --network=host analysis:v1 Rscript /script/analysis.R
```

## auto start scripts

```dockerfile
CMD Rscript /script/start_service.R
```
* resp.

```dockerfile
CMD Rscript /script/analysis.R
```


# Task 4: run biocontainers container

https://biocontainers-edu.readthedocs.io/en/latest/running_example.html

1. Download Blast container

```bash
docker pull biocontainers/blast:2.2.31
```

2. Run container with test

```bash
docker run --rm biocontainers/blast:2.2.31 blastp -help
```

3. Download sample genome

```bash
docker run --rm biocontainers/blast:2.2.31 curl https://www.uniprot.org/uniprot/P04156.fasta >> P04156.fasta
```

4. Download protein sequences and extract

```bash
docker run --rm -v /c/temp/:/data/ biocontainers/blast:2.2.31 curl -O ftp://ftp.ncbi.nih.gov/refseq/D_rerio/mRNA_Prot/zebrafish.1.protein.faa.gz
docker run --rm -v /c/temp/:/data/ biocontainers/blast:2.2.31 gunzip zebrafish.1.protein.faa.gz
```

5. Create Blast Database

```bash
docker run --rm -v /c/temp/:/data/ biocontainers/blast:2.2.31 makeblastdb -in zebrafish.1.protein.faa -dbtype prot
```

6. Run Blast

```bash
docker run --rm -v /c/temp/:/data/ biocontainers/blast:2.2.31 blastp -query P04156.fasta -db zebrafish.1.protein.faa -out results.txt
```