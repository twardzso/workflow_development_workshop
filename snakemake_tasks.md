# Workflow Development Workshop - Snakemake

Examples are from: https://snakemake.readthedocs.io/en/stable/tutorial/tutorial.html (06.02.2020)
 
# Setup

* (optional) start linux docker container

```bash
docker run -it -v /c/temp/:/windows/ ubuntu:bionic  /bin/bash
apt-get update 
apt-get install wget vim
cd /home/
```

* (optional) install miniconda

```language
wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash Miniconda3-latest-Linux-x86_64.sh
PATH="/root/miniconda3/bin:${PATH}"
exec bash
```

* We download data and a conda environment file from the Snakemake repository:

```bash
wget https://github.com/snakemake/snakemake-tutorial-data/archive/v5.4.5.tar.gz
tar -xf v5.4.5.tar.gz --strip 1
```

* Next, we install the environment and activate it:

```bash
conda env create --name snakemake-tutorial --file environment.yaml
conda activate snakemake-tutorial
```

* Setup done

# Basics

##  A simple workflow

* The first workflow executes bwa mem at some input reads and transforms resulting alignment into BAM format.

```python
rule bwa_map:
    input:
        genome="data/genome.fa",
        reads="data/samples/A.fastq"
    output:
        alignment="mapped_reads/A.bam"
    shell:
        "bwa mem {input.genome} {input.reads} | samtools view -Sb - > {output.alignment}"
```

* We create a Snakefile, copy the code into it and then run the workflow

```bash
snakemake -np mapped_reads/A.bam
snakemake mapped_reads/A.bam
```

## Generalization

* Snakemake uses wildcards for generalization; here {sample} is used to generalize input reads:

```python
rule bwa_map:
    input:
        genome="data/genome.fa",
        reads="data/samples/{sample}.fastq"
    output:
        alignment="mapped_reads/{sample}.bam"
    shell:
        "bwa mem {input.genome} {input.reads} | samtools view -Sb - > {output.alignment}"
```

* Run new workflow:

```bash
snakemake -np mapped_reads/B.bam
snakemake -np mapped_reads/A.bam mapped_reads/B.bam
```

* Snakemake will execute sample B only, because we already executed sample A. To rerun sample A we can either "update" reads A or delete alignment A:

```bash
touch data/samples/A.fastq
snakemake -np mapped_reads/A.bam mapped_reads/B.bam
```

## Add more steps

* We add more steps to the workflow; sort alignments using samtools:

```python
rule samtools_sort:
    input:
        bam="mapped_reads/{sample}.bam"
    output:
        bam="sorted_reads/{sample}.bam"
    shell:
        "samtools sort -T sorted_reads/{wildcards.sample} -O bam {input.bam} > {output.bam}"
```

 * and create an alignment index file:

```python
rule samtools_index:
    input:
        bam="sorted_reads/{sample}.bam"
    output:
        bai="sorted_reads/{sample}.bam.bai"
    shell:
        "samtools index {input.bam}"
```

* Lets use Snakemake to visualize the execution DAG

```
snakemake --dag sorted_reads/{A,B}.bam.bai | dot -Tsvg > dag.svg
```

## Final steps

* Lets call some small nucleotide variants:

```python
SAMPLES = ["A", "B"]

rule bcftools_call:
    input:
        fa="data/genome.fa",
        bams=expand("sorted_reads/{sample}.bam", sample=SAMPLES),
        bais=expand("sorted_reads/{sample}.bam.bai", sample=SAMPLES)
    output:
        "calls/all.vcf"
    shell:
        "samtools mpileup -g -f {input.fa} {input.bams} | "
        "bcftools call -mv - > {output}"
```

* And we apply a custom script to visualize the results

```python
rule plot_quals:
    input:
        "calls/all.vcf"
    output:
        "plots/quals.svg"
    script:
        "scripts/plot-quals.py"
```

* Therefore, we need add the python script to "scripts/plot-quals.py":

```python
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
from pysam import VariantFile

quals = [record.qual for record in VariantFile(snakemake.input[0])]
plt.hist(quals)

plt.savefig(snakemake.output[0])
```

* It is good practice to add a final target rule at the beginning at the workflow, which will be executed by default

```python
rule all:
    input:
        "plots/quals.svg"
```

* Run it all!

```
snakemake -n
``` 

# Advanced

## Number of threads

```python
rule bwa_map:
    input:
        genome="data/genome.fa",
        reads="data/samples/{sample}.fastq"
    output:
        alignment="mapped_reads/{sample}.bam"
    threads: 8
    shell:
        "bwa mem -t {threads} {input.genome} {input.reads} | samtools view -Sb - > {output.alignment}"
```

* Snakemake will now use up to 8 core for the execution of a bwa job

```bash
snakemake --forceall --cores 1
snakemake --forceall --cores 8
snakemake --forceall --cores 16
```

## Config file

* Snakemake can use a yaml file for configuration:

```python
configfile: "config.yaml"

samples = [sample["id"] for sample in config["samples"]]

rule bcftools_call:
    input:
        fa="data/genome.fa",
        bams=expand("sorted_reads/{sample}.bam", sample=samples),
        bais=expand("sorted_reads/{sample}.bam.bai", sample=samples)
    output:
        vcf="calls/all.vcf"
    shell:
        "samtools mpileup -g -f {input.fa} {input.bams} | "
        "bcftools call -mv - > {output.vcf}"
```

* Here is an example yaml file:

```yaml
samples:
    - 
        id: A
        path: data/samples/A.fastq
    -
        id: B
        path: data/samples/B.fastq
```

* Snakemake can also validate a config file using a yaml shema:

```yaml
$schema: "http://json-schema.org/draft-06/schema#"
description: This is a schema for config files used by the alignment workflow from eilslabs-workflows.
type: object
properties:
    samples:
        type: array
        description: The workflow can handle processing of multiple samples in parallel.
        items:
            type: object
            description: Each sample has an id a path
            properties:
                id: 
                    type: string
                    description: unique sample identifier
                path: 
                    type: string
                    description: path to fastq file
```

* we need to tell Snakemake to validate the configfile

```python
from snakemake.utils import validate
configfile: "config.yaml"
validate(config, "config.schema.yaml")
```

# input function

* Next, we need to pass the paths from the config file also in mapping rule. Herefore, we use a function as input:

```python
def bwa_map_input(wildcards):
    return next(sample["path"] for sample in config["samples"] if sample["id"] == wildcards.sample)

rule bwa_map:
    input:
        fa="data/genome.fa",
        fastq=bwa_map_input
    output:
        bam="mapped_reads/{sample}.bam"
    threads: 8
    shell:
        "bwa mem -t {threads} {input.fa} {input.fastq} | samtools view -Sb - > {output.bam}"
```

## Parameter

* Snakemake can also pass additional parameter depending on current wildcards

```python
rule bwa_map:
    input:
        fa="data/genome.fa",
        fastq=bwa_map_input
    output:
        bam="mapped_reads/{sample}.bam"
    params:
        rg=r"@RG\tID:{sample}\tSM:{sample}"
    threads: 8
    shell:
        "bwa mem -R '{params.rg}' -t {threads} {input.fa} {input.fastq} | samtools view -Sb - > {output.bam}"
```

## Temporary & protected files

* Output files can be marked as temporary or protected;
    - temporary files will be delteted by snakemake after workflow exection
    - protected files will be marked as write protected

```python
rule bwa_map:
    input:
        fa="data/genome.fa",
        fastq=bwa_map_input
    output:
        bam=temp("mapped_reads/{sample}.bam")
    params:
        rg=r"@RG\tID:{sample}\tSM:{sample}"
    threads: 8
    shell:
        "bwa mem -R '{params.rg}' -t {threads} {input.fa} {input.fastq} | samtools view -Sb - > {output.bam}"

rule samtools_sort:
    input:
        bam="mapped_reads/{sample}.bam"
    output:
        bam=protected("sorted_reads/{sample}.bam")
    shell:
        "samtools sort -T sorted_reads/{wildcards.sample} -O bam {input.bam} > {output.bam}"
```

## pipes

* Output files can also be defined as named pipes

```python

rule bwa_map:
    input:
        fa="data/genome.fa",
        fastq=bwa_map_input
    output:
        bam=pipe("mapped_reads/NP_{sample}.bam")
    params:
        rg=r"@RG\tID:{sample}\tSM:{sample}"
    threads: 8
    shell:
        "sleep 60; bwa mem -R '{params.rg}' -t {threads} {input.fa} {input.fastq} | samtools view -Sb - > {output.bam}"

rule samtools_sort:
    input:
        bam="mapped_reads/NP_{sample}.bam"
    output:
        bam=protected("sorted_reads/{sample}.bam")
    shell:
        "samtools sort -T sorted_reads/{wildcards.sample} -O bam {input.bam} > {output.bam}"
```
## wrapper?